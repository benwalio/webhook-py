from django.apps import AppConfig


class LupHooksConfig(AppConfig):
    name = 'lup_hooks'
