from django.conf.urls import include, url, patterns
from django.contrib import admin

from lup_hooks import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^webhook', views.webhook, name='webhook'),
]
